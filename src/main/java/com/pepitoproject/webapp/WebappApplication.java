package com.pepitoproject.webapp;

import com.pepitoproject.webapp.utilities.uploads.UploadFileService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebappApplication implements CommandLineRunner {

    private final UploadFileService uploadFileService;

    public WebappApplication(UploadFileService uploadFileService) {
        this.uploadFileService = uploadFileService;
    }

    public static void main(String[] args) {
        SpringApplication.run(WebappApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        uploadFileService.deleteAll();
        uploadFileService.init();
    }
}
