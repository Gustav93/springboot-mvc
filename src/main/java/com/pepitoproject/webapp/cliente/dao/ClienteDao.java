package com.pepitoproject.webapp.cliente.dao;

import com.pepitoproject.webapp.cliente.model.Cliente;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClienteDao extends PagingAndSortingRepository<Cliente, Long> {
}
