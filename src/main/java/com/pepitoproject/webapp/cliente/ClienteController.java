package com.pepitoproject.webapp.cliente;

import com.pepitoproject.webapp.cliente.model.Cliente;
import com.pepitoproject.webapp.cliente.service.ClienteService;
import com.pepitoproject.webapp.utilities.paginator.PageRender;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller()
@RequestMapping({"/","cliente/"})
public class ClienteController {

    private final ClienteService clienteService;

    public ClienteController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping({"/","/listar"})
    public String listar(@RequestParam(defaultValue = "0") int page, Model model) {
        Pageable pageRequest = PageRequest.of(page, 10);
        Page<Cliente> clientes = clienteService.findAll(pageRequest);
        PageRender<Cliente> pageRender = new PageRender<>("listar", clientes);
        model.addAttribute("clientes", clientes);
        model.addAttribute("page", pageRender);
        return "listar";
    }

    @GetMapping("/form")
    public String crear(Map<String, Object> model) {
        model.put("cliente", new Cliente());
        return "form";
    }

    @PostMapping("/form")
    public String guardar(Cliente cliente) {
        clienteService.save(cliente);
        return "redirect:listar";
    }
}