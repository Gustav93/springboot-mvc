package com.pepitoproject.webapp.cliente.model;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private long id;

    @NotEmpty
    private String nombre;
    @NotEmpty
    private String apellido;
    @Email
    private String email;

    @NotNull
    @Column(name = "create_at")
    private LocalDateTime createAt;

    @PrePersist
    private void prePersist(){
        this.createAt = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(LocalDateTime createAt) {
        this.createAt = createAt;
    }
}
