package com.pepitoproject.webapp.cliente.service;

import com.pepitoproject.webapp.cliente.model.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ClienteService {
    List<Cliente> findAll();

    Page<Cliente> findAll(Pageable pageable);

    Cliente findById(long id);

    void save(Cliente cliente);

    void delete(Cliente cliente);

    void delete(long id);

}
