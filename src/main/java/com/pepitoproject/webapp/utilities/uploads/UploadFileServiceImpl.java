package com.pepitoproject.webapp.utilities.uploads;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    private static final String UPLOAD_FOLDER = "uploads";

    private static final Logger LOG = LoggerFactory.getLogger(UploadFileServiceImpl.class);

    @Override
    public Resource load(String filename) throws MalformedURLException {
        Path pathImagen = getPath(filename);
        Resource resource = new UrlResource(pathImagen.toUri());

        if(!resource.exists() || !resource.isReadable()){
            throw new RuntimeException(String.format("Error: No se puede cargar la imagen %s", pathImagen.toString()));
        }

        return resource;
    }

    @Override
    public String save(MultipartFile file) throws IOException {
        String uniqueFilename = String.format("%s_%s", UUID.randomUUID().toString(), file.getOriginalFilename());
        Path rootPath = getPath(uniqueFilename);
        Files.copy(file.getInputStream(), rootPath);
        return uniqueFilename;
    }

    @Override
    public boolean delete(String filename) {
        File file = getPath(filename).toFile();

        if(file.exists() && file.canRead()) {
            return file.delete();
        }
        return false;
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(Paths.get(UPLOAD_FOLDER).toFile());
    }

    @Override
    public void init() throws IOException {
        if(!Files.exists(Paths.get(UPLOAD_FOLDER)))
            Files.createDirectory(Paths.get(UPLOAD_FOLDER));
    }

    public Path getPath(String filename) {
        return Paths.get(UPLOAD_FOLDER).resolve(filename).toAbsolutePath();
    }
}
