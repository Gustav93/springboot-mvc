package com.pepitoproject.webapp.utilities.paginator;

import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

public class PageRender<T> {
    private final String url;
    private final Page<T> page;
    private final int totalPages;
    private final int currentPage;
    private final List<PageItem> pages;

    public PageRender(String url, Page<T> page) {
        int start, end;
        this.url = url;
        this.page = page;
        this.pages = new ArrayList<>();
        int pageSize = 3; //es la cantidad de paginas que se muestran en el paginador
        totalPages = page.getTotalPages();
        currentPage = page.getNumber()+1;

        if(totalPages <= pageSize) {
            start = 1;
            end = totalPages;
        }else {
            if(currentPage <= pageSize /2){
                start = 1;
                end = pageSize;
            } else if(currentPage >= totalPages - pageSize /2 ) {
                start = totalPages - pageSize + 1;
                end = pageSize;
            } else {
                start = currentPage - pageSize /2;
                end = pageSize;
            }
        }

        for (int i=0; i<end; i++) {
            pages.add(new PageItem(start+i, currentPage == start+i));
        }

    }

    public String getUrl() {
        return url;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public List<PageItem> getPages() {
        return pages;
    }

    public boolean isFirst() {
        return page.isFirst();
    }

    public boolean isLast() {
        return page.isLast();
    }

    public boolean isHasNext() {
        return page.hasNext();
    }

    public boolean isHasPrevious() {
        return page.hasPrevious();
    }
}
