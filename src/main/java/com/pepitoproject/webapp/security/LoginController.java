package com.pepitoproject.webapp.security;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.time.LocalDate;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String login(@RequestParam(required = false) String error,
                        @RequestParam(required = false) String logout, Model model, Principal principal, RedirectAttributes flash){

        if(principal!=null){
            flash.addFlashAttribute("info", "Ya se ha iniciado la sesion anteriormente");
            return "redirect:/";
        }
        if(error!=null){
            model.addAttribute("error", "Nombre de usuario o contraseña incorrectos, vuelva a intentarlo");
        }
        if(logout!=null) {
            model.addAttribute("success", "Se ha cerrado sesión correctamente");
        }
        model.addAttribute("localDate", LocalDate.now());
        return "/login/login";
    }
}
