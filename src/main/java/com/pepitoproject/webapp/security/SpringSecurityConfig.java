package com.pepitoproject.webapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    private final DefaultAuthenticationSuccessHandler authenticationSuccessHandler;

    private final DefaultLogoutSuccessHandler logoutSuccessHandler;

    private final DefaultLoginFailureHandler loginFailureHandler;

    public SpringSecurityConfig(@Qualifier("usuarioServiceImpl") UserDetailsService userDetailsService, DefaultAuthenticationSuccessHandler authenticationSuccessHandler, DefaultLogoutSuccessHandler logoutSuccessHandler, DefaultLoginFailureHandler loginFailureHandler) {
        this.userDetailsService = userDetailsService;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
        this.logoutSuccessHandler = logoutSuccessHandler;
        this.loginFailureHandler = loginFailureHandler;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/images/**","/login**").permitAll()
                .antMatchers("/").hasAnyRole("USER", "ADMIN")
                .antMatchers("/usuarios/mi-perfil").hasAnyRole("ADMIN", "USER")
                .antMatchers("/usuarios/**").hasAnyRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .permitAll()
                    .successHandler(authenticationSuccessHandler)
                    .failureHandler(loginFailureHandler)
                .and()
                    .logout()
                    .permitAll()
                    .logoutSuccessHandler(logoutSuccessHandler);
    }

    @Autowired
    public void configurerGlobal(AuthenticationManagerBuilder builder) throws Exception {
        PasswordEncoder passwordEncoder = passwordEncoder();
        builder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }
}
