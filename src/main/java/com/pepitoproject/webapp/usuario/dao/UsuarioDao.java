package com.pepitoproject.webapp.usuario.dao;

import com.pepitoproject.webapp.usuario.model.Usuario;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UsuarioDao extends PagingAndSortingRepository <Usuario, Long> {

    Usuario findUsuarioByUsername(String username);
}
