package com.pepitoproject.webapp.usuario.dao;

import com.pepitoproject.webapp.usuario.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {
}
