package com.pepitoproject.webapp.usuario.service;

import com.pepitoproject.webapp.usuario.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAll();
}
