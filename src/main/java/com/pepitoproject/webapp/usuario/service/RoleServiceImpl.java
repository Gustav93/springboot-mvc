package com.pepitoproject.webapp.usuario.service;

import com.pepitoproject.webapp.usuario.model.Role;
import com.pepitoproject.webapp.usuario.dao.RoleDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;

    public RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<Role> findAll() {
        return roleDao.findAll();
    }
}
