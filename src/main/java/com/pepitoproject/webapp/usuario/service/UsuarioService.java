package com.pepitoproject.webapp.usuario.service;

import com.pepitoproject.webapp.usuario.model.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UsuarioService {

    Usuario findUsuarioByUsername(String username);

    List<Usuario> findAll();

    Page<Usuario> findAll(Pageable pageable);

    void save(Usuario usuario);

    void delete(Usuario usuario);

    void delete(long id);

    Usuario findById(Long id);
}
