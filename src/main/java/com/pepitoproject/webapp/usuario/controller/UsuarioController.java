package com.pepitoproject.webapp.usuario.controller;

import com.pepitoproject.webapp.usuario.model.Usuario;
import com.pepitoproject.webapp.usuario.service.RoleService;
import com.pepitoproject.webapp.usuario.service.UsuarioService;
import com.pepitoproject.webapp.utilities.paginator.PageRender;
import com.pepitoproject.webapp.utilities.uploads.UploadFileService;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Controller
@RequestMapping("usuarios/")
public class UsuarioController {

    private final UsuarioService usuarioService;

    private final PasswordEncoder passwordEncoder;

    private final RoleService roleService;

    private final UploadFileService uploadFileService;

    public UsuarioController(UsuarioService usuarioService, PasswordEncoder passwordEncoder, RoleService roleService, UploadFileService uploadFileService) {
        this.usuarioService = usuarioService;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
        this.uploadFileService = uploadFileService;
    }

    @GetMapping("/listar")
    public String listar(@RequestParam(defaultValue = "0") int page, Model model){
        Pageable pageRequest = PageRequest.of(page, 5);
        Page<Usuario> usuarios = usuarioService.findAll(pageRequest);
        PageRender<Usuario> pageRender = new PageRender<>("listar", usuarios);
        model.addAttribute("usuarios", usuarioService.findAll(pageRequest));
        model.addAttribute("page", pageRender);
        return "usuarios/listar";
    }

    @GetMapping("/crear")
    public String crear(Model model){
        model.addAttribute("usuario", new Usuario());
        model.addAttribute("roles", roleService.findAll());
        return "usuarios/form-crear";
    }

    @PostMapping("/crear")
    public String guardar(@Valid Usuario usuario, BindingResult result, Model model, RedirectAttributes flash){
        if(result.hasErrors()){
            model.addAttribute("titulo", "Crear Usuario");
            model.addAttribute("roles", roleService.findAll());
            return "usuarios/form-crear";
        }

        flash.addFlashAttribute("success", "Usuario Creado con exito");
        usuarioService.save(usuario);
        return "redirect:listar";
    }

    @GetMapping("/editar/{username}")
    public String editar(Model model, @PathVariable String username, RedirectAttributes flash) {
        Usuario user;

        if(username==null) {
            flash.addFlashAttribute("error", "El usuername es inválido");
            return "redirect:listar";
        }
        user = usuarioService.findUsuarioByUsername(username);

        if(user==null){
            flash.addFlashAttribute("error", "El usuario no existe");
            return "redirect:listar";
        }
        model.addAttribute("usuario", user);
        model.addAttribute("roles", roleService.findAll());
        return "usuarios/form";
    }

    @PostMapping("/editar")
    public String editar(@Valid Usuario usuario,
                         @RequestParam(required = false) MultipartFile file, BindingResult result, Model model, RedirectAttributes flash) {
        Usuario usuarioDb;
        String uniqueFilename;
        if(result.hasErrors()){
            model.addAttribute("titulo", "Editar Usuario");
            model.addAttribute("roles", roleService.findAll());
            return "usuarios/form";
        }
        usuarioDb = usuarioService.findById(usuario.getId());

        try {

            if(file==null){
                usuario.setImagen(usuarioDb.getImagen());
            } else if(!file.isEmpty()&&usuarioDb.getImagen()!=null&&!usuarioDb.getImagen().isEmpty()){
                uploadFileService.delete(usuarioDb.getImagen());
                uniqueFilename = uploadFileService.save(file);
                usuario.setImagen(uniqueFilename);

            }else if(!file.isEmpty()&&usuarioDb.getImagen()!=null&&usuarioDb.getImagen().isEmpty()){
                uniqueFilename = uploadFileService.save(file);
                usuario.setImagen(uniqueFilename);
            }else{
                usuario.setImagen(usuarioDb.getImagen());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        if(!usuarioDb.getPassword().equals(usuario.getPassword())){
            usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
        }
        usuarioService.save(usuario);
        flash.addFlashAttribute("success", "Usuario editado correctamente");
        return "redirect:listar";
    }

    @GetMapping("/eliminar/{id}")
    public String eliminar(@PathVariable Long id, RedirectAttributes flash) {
        Usuario usuarioDb = usuarioService.findById(id);

        Path rootPath = Paths.get("uploads").resolve(usuarioDb.getImagen()).toAbsolutePath();
        File file = rootPath.toFile();

        if(file.exists()&&file.canRead()){
            file.delete();
        }

        usuarioService.delete(id);
        flash.addFlashAttribute("success", "Usuario eliminado correctamente");

        return "redirect:../listar";
    }

    @GetMapping("/mi-perfil")
    public String miPerfil(Principal principal, Model model) {
        Usuario user = usuarioService.findUsuarioByUsername(principal.getName());
        model.addAttribute("usuario", user);
        model.addAttribute("roles", roleService.findAll());
        return "usuarios/form-mi-perfil";
    }

    @PostMapping("/mi-perfil")
    public String editarMiPerfil(@Valid Usuario usuario,
                                 @RequestParam MultipartFile file, BindingResult result, Model model, RedirectAttributes flash) {
        final String pathMiPerfil = "usuarios/form-mi-perfil";
        if(result.hasErrors()){
            model.addAttribute("roles", roleService.findAll());
            return pathMiPerfil;
        }
        editar(usuario, file, result, model, flash);
        model.addAttribute("roles", roleService.findAll());
        model.addAttribute("info", "Perfil actualizado");
        return pathMiPerfil;
    }

    @GetMapping("/imagenes/{filename:.+}")
    public ResponseEntity<Resource> verImagen(@PathVariable String filename) {
        Resource resource = null;
        try {
           resource = uploadFileService.load(filename);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", resource.getFilename()))
                .body(resource);
    }
}
